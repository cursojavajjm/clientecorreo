package com.cursojava.clientecorreo.services.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtil {
	
	private static EntityManager em;
	
	public static EntityManager getEntityManager() {
		if (em == null) {
			EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("default");
			em = emFactory.createEntityManager();
		}
		return em;
	}

}
