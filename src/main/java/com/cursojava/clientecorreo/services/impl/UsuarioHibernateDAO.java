package com.cursojava.clientecorreo.services.impl;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cursojava.clientecorreo.models.Usuario;
import com.cursojava.clientecorreo.services.IUsuarioDAO;

public class UsuarioHibernateDAO implements IUsuarioDAO {
	
	private EntityManager em;
	
	public UsuarioHibernateDAO(EntityManager em) {
		this.em = em;
	}

	/**
	 * Recupera un Usuario de la BD
	 * 
	 * NOTA: Las transacciones (transaction) es un metodo usado
	 * en Bases de Datos para proteger los datos de posibles errores.
	 * 
	 * Sirve para encapsular una operacion y la base de datos solo
	 * se modificara cuando se haga un commit de la transaccion
	 * 
	 * En Hibernate usamos esto siempre para operar con bases de datos
	 */
	public Usuario recuperar(int id) {
		try {
			this.em.getTransaction().begin();
			Usuario usuario = this.em.find(Usuario.class, id);
			this.em.getTransaction().commit();
			return usuario;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * El metodo persiste(Object o) almacena (persiste) el
	 * objeto en la base de datos
	 */
	public boolean guardar(Usuario u) {
		try {
			this.em.getTransaction().begin();
			this.em.persist(u);
			this.em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public Usuario validar(String email, String password) {
		try {
			this.em.getTransaction().begin();
			
//			this.em.createNativeQuery("SELECT * FROM usuarios");
			Query query = this.em.createNamedQuery("from usuarios where email = ? and password = ?");
			query.setParameter(1, email);
			query.setParameter(2, password);
			query.executeUpdate();
			Usuario u = (Usuario) query.getSingleResult();
			
			this.em.getTransaction().commit();
			return u;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
