package com.cursojava.clientecorreo.services;

import com.cursojava.clientecorreo.models.Usuario;

public interface IUsuarioDAO {
	
	Usuario recuperar(int id);
	Usuario validar(String email, String password);
	boolean guardar(Usuario u);

}
