package com.cursojava.clientecorreo.controllers;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cursojava.clientecorreo.models.Usuario;
import com.cursojava.clientecorreo.services.IUsuarioDAO;
import com.cursojava.clientecorreo.services.impl.HibernateUtil;
import com.cursojava.clientecorreo.services.impl.UsuarioHibernateDAO;

@Controller
public class UserController {
	
	private IUsuarioDAO usuarioService;
	
	public UserController() {
		EntityManager em = HibernateUtil.getEntityManager();
		this.usuarioService = new UsuarioHibernateDAO(em);
	}
	
	@RequestMapping("/login")
	public ModelAndView logIn(@RequestParam String usuario, @RequestParam String password) {
		Usuario u = this.usuarioService.validar(usuario, password);
		ModelAndView mv = null;
		if (u == null) {
			mv = new ModelAndView("error");
			mv.addObject("error", "Usuario y/o contraseņa invalidos");
		} else {
			mv = new ModelAndView("home");
			mv.addObject("usuario", u);
		}
		return mv;
	}
	
	@RequestMapping("/usuario/registrar")
	public ModelAndView registrar(@RequestParam String nombre, @RequestParam String apellidos,
		@RequestParam String email, @RequestParam String password) {
		ModelAndView mv = new ModelAndView();
		// TODO
		return mv;
	}
	
	@RequestMapping("/usuario/perfil")
	public ModelAndView perfil(@RequestParam int id) {
		Usuario usuario = usuarioService.recuperar(id);
		ModelAndView mv = new ModelAndView("perfil");
		mv.addObject("usuario", usuario);
		return mv;
	}

}
