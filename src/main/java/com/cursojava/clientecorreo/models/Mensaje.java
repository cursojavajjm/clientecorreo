package com.cursojava.clientecorreo.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mensaje")
public class Mensaje {
	
	@Id
	@Column(name = "mensaje_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne()
	@JoinColumn(name = "usuario_id")
	private Usuario remitente;
	private String emailDestinatario;
	private String asunto;
	private String cuerpo;
	private Date fecha;
	
	public Mensaje() {
	}

	public Mensaje(int id, Usuario remitente, String emailDestinatario, String asunto, String cuerpo, Date fecha) {
		super();
		this.id = id;
		this.remitente = remitente;
		this.emailDestinatario = emailDestinatario;
		this.asunto = asunto;
		this.cuerpo = cuerpo;
		this.fecha = fecha;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Usuario getRemitente() {
		return remitente;
	}

	public void setRemitente(Usuario remitente) {
		this.remitente = remitente;
	}

	public String getEmailDestinatario() {
		return emailDestinatario;
	}

	public void setEmailDestinatario(String emailDestinatario) {
		this.emailDestinatario = emailDestinatario;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "Mensaje [id=" + id + ", remitente=" + remitente + ", asunto="
				+ asunto + ", cuerpo=" + cuerpo + ", fecha=" + fecha + "]";
	}

}
