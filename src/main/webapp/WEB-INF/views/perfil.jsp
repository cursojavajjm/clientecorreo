<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cliente de Correo - Perfil de usuario</title>
</head>
<body>
	<center>
		<h1>Perfil de ${usuario.nombre} ${usuario.apellidos}</h1>
		<p>Nombre: ${usuario.nombre}</p>
		<p>Apellidos: ${usuario.apellidos}</p>
		<p>Email: ${usuario.email}</p>
	</center>
</body>
</html>