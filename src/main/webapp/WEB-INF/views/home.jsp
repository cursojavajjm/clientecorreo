<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cliente de correo</title>
</head>
<body>
	<h1>Cliente de correo</h1>
	
	<div class="menu">
		<ol>
			<li><a href="/mensajes/enviar">Enviar</a></li>
			<li><a href="/mensajes/recibidos">Recibidos</a></li>
			<li><a href="/mensajes/enviados">Enviados</a></li>
			<li><a href="/usuario/perfil?id=${usuario.id}">Mi perfil</a></li>
		</ol>
	</div>

</body>
</html>